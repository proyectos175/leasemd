import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputComponent } from '@input';
import { ContactComponent } from '@form/contact';
import { InputDirectiveModule } from '../directive/input-directive/input.directive.module';

@NgModule({
    declarations: [ContactComponent, InputComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule, InputDirectiveModule],
    exports: [ContactComponent, InputComponent]
})
export class ComponentsModule {}
