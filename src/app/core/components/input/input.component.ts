import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'leasemd-input',
    templateUrl: './input.component.html'
})
export class InputComponent implements OnInit {
    @Input()
    public isValid: boolean | undefined = true;

    constructor() {}

    ngOnInit(): void {
        console.log(this.isValid);
    }
}
