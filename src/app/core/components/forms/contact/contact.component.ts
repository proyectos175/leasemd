import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DistribuidorService } from '@services/distribuidor/distribuidor.service';
import { Distributor } from '@interfaces/distributor/distributor';
import { Observable } from 'rxjs';

@Component({
    selector: 'leasemd-form-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
    public loading: boolean = false;
    public form: FormGroup = new FormGroup({});
    public listDistributors$: Observable<Array<Distributor>> = new Observable<Array<Distributor>>();
    public ready: boolean = false;

    constructor(private formBuilder: FormBuilder, private distribuidorService: DistribuidorService) {
        this.formBuild();
    }

    ngOnInit(): void {
        this.listDistributors$ = this.distribuidorService.getListDistributors();
    }

    public submit(event: Event): void {
        event.preventDefault();
        if (this.ready) {
            this.ready = false;
            return;
        }

        this.loading = true;
        if (this.form.valid) {
            this.distribuidorService.createSolicited(this.form.value).subscribe((response) => {
                if (response) {
                    this.loading = false;
                    this.ready = true;
                    this.form.reset();
                }
            });
        } else {
            this.form.markAllAsTouched();
        }
    }

    private formBuild(): void {
        this.form = this.formBuilder.group({
            idDistribuidor: ['', Validators.required],
            paginaAuto: ['', Validators.required],
            nombreCompleto: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]]
        });
    }
}
