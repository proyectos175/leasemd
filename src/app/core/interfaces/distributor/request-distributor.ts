export interface RequestDistributor {
    idDistribuidor: number;
    paginaAuto: string;
    nombreCompleto: string;
    email: string;
}
