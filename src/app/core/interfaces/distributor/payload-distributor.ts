import { Distributor } from './distributor';

export interface PayloadDistributor {
    distribuidores: Array<Distributor>;
}
