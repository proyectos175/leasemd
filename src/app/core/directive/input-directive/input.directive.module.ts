import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputDirective } from './input.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [InputDirective],
    imports: [CommonModule, FormsModule, ReactiveFormsModule],
    exports: [InputDirective],
    providers: [InputDirective]
})
export class InputDirectiveModule {}
