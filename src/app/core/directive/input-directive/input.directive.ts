import { Directive, ElementRef, HostBinding, HostListener, OnInit } from '@angular/core';
import { NgControl, NgModel } from '@angular/forms';

@Directive({
    selector: '[leasemdInput]'
})
export class InputDirective implements OnInit {
    constructor(private el: ElementRef, private control: NgControl) {}

    @HostBinding('class.ng-untouched') get untouched() {
        return this.control.untouched;
    }

    @HostBinding('class.ng-invalid') get invalid() {
        return this.control.invalid;
    }

    @HostBinding('class.ng-valid') get valid() {
        return this.control.valid;
    }

    @HostBinding('class.ng-dirty') get dirty() {
        return this.control.dirty;
    }

    @HostBinding('class.ng-pristine') get pristine() {
        return this.control.pristine;
    }

    @HostListener('blur') listenerBlur() {
        if (this.valid) {
        } else if (this.invalid) {
            this.el.nativeElement.parentElement.classList.add('input-field-danger');
        }
    }

    @HostListener('keydown') listenerKey() {
        this.el.nativeElement.parentElement.classList.remove('input-field-danger');
    }

    ngOnInit(): void {

    }

    private addInputInvalid(): void {
        console.log(this.el.nativeElement.parentElement);
    }
}
