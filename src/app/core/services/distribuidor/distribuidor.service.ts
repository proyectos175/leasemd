import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Distributor } from '@interfaces/distributor/distributor';
import { PayloadDistributor } from '@interfaces/distributor/payload-distributor';
import { RequestDistributor } from '@interfaces/distributor/request-distributor';

const ulr = environment.url;
@Injectable({
    providedIn: 'root'
})
export class DistribuidorService {
    constructor(private http: HttpClient) {}

    public getListDistributors(): Observable<Array<Distributor>> {
        return this.http.get<PayloadDistributor>(`${ulr}/distribuidores`).pipe(
            map((response: PayloadDistributor) => {
                return response.distribuidores;
            })
        );
    }

    public createSolicited(body: RequestDistributor): Observable<any> {
        return this.http
            .post(`${ulr}/cotizacion`, body)
            .pipe(catchError((error) => of(error.status === 200)));
    }
}
