import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from '@layout/main';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/module';

@NgModule({
    declarations: [MainComponent],
    imports: [CommonModule, SharedModule, RouterModule],
    exports: [MainComponent]
})
export class LayoutModule {}
