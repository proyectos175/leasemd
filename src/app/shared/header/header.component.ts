import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'leasemd-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public viewDropdown: boolean = false;
    public viewMenu: boolean = false;

    constructor() {}

    ngOnInit(): void {}

    public openDropdown(): void {
        this.viewDropdown = !this.viewDropdown;
    }

    public openMenu(): void {
        this.viewMenu = !this.viewMenu;
    }
}
