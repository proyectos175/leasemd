import { Component } from '@angular/core';

@Component({
    selector: 'leasemd-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'leasemd';
}
